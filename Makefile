# Makefile for makedemo
PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

OBJS = P3_MakeDemo.o ma.o
CXX = g++
CC = gcc
BUILD_MODE = debug

ifeq ($(BUILD_MODE),debug)
    CFLAGS += -g
else ifeq ($(BUILD_MODE),run)
    CFLAGS += -O2
else
    $(error Build mode $(BUILD_MODE) not supported by this Makefile)
endif

all: P3_MakeDemo

P3_MakeDemo:    $(OBJS)
    $(CXX) -o $@ $^

%.o:    $(PROJECT_ROOT)%.cpp
    $(CXX) -c $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

%.o:    $(PROJECT_ROOT)%.c
    $(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

clean:
    rm -fr P3_MakeDemo $(OBJS)
    make all 
gcc -c -g  -o P3_MakeDemo.o /project/P3_MakeDemo/P3_MakeDemo.c
gcc -c -g  -o ma.o /project/P3_MakeDemo/ma.c
g++ -o P3_MakeDemo P3_MakeDemo.o ma.o
